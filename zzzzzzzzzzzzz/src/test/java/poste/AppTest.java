package poste;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest ;
 import org.junit.jupiter. params . provider .Arguments; 
import poste.Lettre;
import poste.Recommandation;
import poste.SacPostal;
import poste.Colis;
import poste.ColisExpress;
import poste.ColisExpressInvalide;

import org.junit.jupiter.params.provider.MethodSource; 
import org.junit.jupiter.api.BeforeEach;

public class AppTest {
	Lettre lettre1;
	 Lettre lettre2;
	 Colis colis1;
	 private static float tolerancePrix=0.001f;
	 private static float toleranceVolume=0.0000001f;	
	 SacPostal sac2;
	SacPostal sac1 ; 
	ColisExpress col;
	
	@BeforeEach    
	public void setup () throws ColisExpressInvalide {   //avec un beforeall les methodes doivent etre startic et les attributs aussi 
		
		 lettre1 = new Lettre("Le pere Noel","famille Kirik, igloo 5, banquise nord","7877", 25, 0.00018f, Recommandation.un, false);
		
		 lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		 colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200); 
		 sac1= new SacPostal();
		  sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);
		sac2 = sac1.extraireV1("7877") ; 
		col=new ColisExpress("jfbhe", "ihd", "jhbf",10, 10,Recommandation.un, "ehfr", 20, false);
  }
	
	
	@Test 
	public void stringtest () 
	{
		assertEquals(lettre1.toString() , "Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire" , "ok ");
	}
	
	@Test 
	public void stringtest2 () 
	{
		assertEquals(lettre2.toString() , "Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence" ,"ok ");
	}
	@Test 
	public void stringtest3() 
	{
		assertEquals(colis1.toString() , "Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0");
	}
	
	@Test 
	public void testaff()
	{
		assertEquals(1.0,lettre1.tarifAffranchissement(),tolerancePrix);
		assertEquals(2.3,lettre2.tarifAffranchissement(),tolerancePrix);
		assertEquals(3.5,colis1.tarifAffranchissement(),tolerancePrix);
	}
	@Test
	public void testremb()
	{
		assertEquals(1.5,lettre1.tarifRemboursement(),tolerancePrix); 
		assertTrue ( Math.abs(lettre2.tarifRemboursement()-15.0f)<tolerancePrix);
	}

	@BeforeEach 
    public void  testsac () 
    {  
    }

	//@Test
	//public void testsacremb ()   
//	{  assertEquals(116.5, sac1.valeurRemboursement(), tolerancePrix);   // la comparaison entre floatant n'est pas vraiment exacte danx on utilise le assertequal avec 3 parametres   (valeur cherch" , valeur actuel , delta prée 
 //   }
	//@Test
	//public void testvolume ( )
	//{ 
	//	assertEquals( 0.025359999558422715,sac1.getVolume(),toleranceVolume);   // ca ne marche pas parceque dans setup() on a extrait un element du sac1  (sac2= sac1.extrairev1("   ") )
		//assertEquals(0.02517999955569394,sac2.getVolume(), toleranceVolume );
	//}
	
	@Test
	public void volumeexception() {   // verifier la leve correcte d'une exception 
		assertThrows (ColisExpressInvalide.class , () ->  new ColisExpress("jfbhe", "iuhd", "jhbf", 50, 30,Recommandation.un, "ehfr", 20, false)  );
        	
	}
	
	//@Test 
	//public void  testnbcolisexpress() {
	//	assertEquals(col.getNbColisExpress(),1);  // pour chaque methode de test on un before ech donc on aura plusiere nbde colis express et comme nb de colis expresse est static on aura 5 ou 6 colisexpress 
//	}
	 
}
	
	
	
	
	
	
	
	


